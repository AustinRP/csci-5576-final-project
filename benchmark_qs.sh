#!/bin/bash

#SBATCH --time=0:30:00     # walltime, abbreviated by -t
#SBATCH --nodes=1          # number of cluster nodes, abbreviated by -N
#SBATCH -o qsbenchmark-%j.out     # name of the stdout redirection file, using the job number (%j)
#SBATCH -e qsbenchmark-%j.err     # name of the stderr redirection file
#SBATCH --ntasks 24         # number of parallel process
#SBATCH --qos debug # quality of service/queue (See QOS section on CU RC guide)

# Setup

source config.summit.rc

echo -e "\nStrong Scaling\n"

echo "num_threads n wall_clock_time"

for NUM_THREADS in 1 2 4 8 12 16 20 24
do
    for PROBLEM_SIZE in 1000000 10000000
    do
        let "X_SIZE = $NUM_THREADS * $PROBLEM_SIZE"
        for REPETITIONS in {1..5} 
        do
            echo "rep=$REPETITIONS"
            OMP_NUM_THREADS=$NUM_THREADS ./test_quicksort.exe $X_SIZE 
        done
    done
done

echo -e "\nWeak Scaling\n"

echo "num_threads n wall_clock_time"

for NUM_THREADS in 1 2 4 8 12 16 20 24
do
    for X_SIZE in 1000000 10000000
    do
        for REPETITIONS in {1..5} 
        do
            echo "rep=$REPETITIONS"
            OMP_NUM_THREADS=$NUM_THREADS ./test_quicksort.exe $X_SIZE 
        done
    done
done
