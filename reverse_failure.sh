#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH -o revs-cnn-%j.out
#SBATCH -e revs-cnn-%j.err
#SBATCH --qos debug

# advise task manager that maximum of 4
# tasks/processes may be spawned
#SBATCH --ntasks 1

module purge
source config.summit.rc

# run the program

echo "checkpoint_rate,failure_rate,sleep_time,num_failures,total_time_lost,elapsed"

for CHECKPOINT_RATE in 5 4 3 2 1
do
    for FAILURE_RATE in 0 4 8 12
    do
        mpirun -n 1 ./chk_lenet 100 $CHECKPOINT_RATE $FAILURE_RATE 1000
    done
done

