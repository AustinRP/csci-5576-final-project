#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH -o nochk-cnn-%j.out
#SBATCH -e nochk-cnn-%j.err
#SBATCH --qos debug

# advise task manager that maximum of 4
# tasks/processes may be spawned
#SBATCH --ntasks 24

module purge
source config.summit.rc

# run the program
echo -e "\nStrong Scaling\n"

echo "num_nodes n wall_clock_time"

for NUM_NODES in 1 4 8 12 16 20 24
do
    for PROBLEM_SIZE in 100 1000
    do
        let "X_SIZE = $NUM_NODES * $PROBLEM_SIZE"
        mpirun -n $NUM_NODES ./lenet_mpi $X_SIZE
    done
done

echo -e "\nWeak Scaling\n"

echo "num_nodes n wall_clock_time"

for NUM_NODES in 1 2 4 8 12 16 20 24
do
    for X_SIZE in 1000000 10000000
    do
        mpirun -n $NUM_NODES ./lenet_mpi $X_SIZE
    done
done
