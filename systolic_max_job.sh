#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH -o sysmax-%j.out
#SBATCH -e sysmax-%j.err
#SBATCH --qos debug

# advise task manager that maximum of 4
# tasks/processes may be spawned
#SBATCH --ntasks 4

# run the program
mpirun -n 4 ./systolic-max.exe array.txt
