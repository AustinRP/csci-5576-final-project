// uniform_int_distribution
#include <iostream>
#include <random>
#include <chrono>

int main()
{
  const int nrolls = 10; // number of experiments
  const int nstars = 95;    // maximum number of stars to distribute

  std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
  std::uniform_int_distribution<int> distribution(0,99);

  int p[10]={};

  for (int i=0; i<nrolls; ++i) {
    int number = distribution(generator);
    std::cout << number << std::endl;
  }


  return 0;
}
