CXX = mpicxx
CXXFLAGS = -std=c++11 -fopenmp

OBJS = lenet.o lenet_mpi.o chk_lenet.o
#TRGS = lenet lenet_mpi chk_lenet
TRGS = chk_lenet lenet_mpi

all: ${TRGS}

%.exe: %.o ${OBJS}
	${CXX} ${CXXFLAGS} ${CPPFLAGS} ${LDFLAGS} $^ ${LDLIBS} -o $@

.SECONDARY:
clean:
	-${RM} *.o ${TRGS} *.exe
